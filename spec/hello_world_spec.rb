require_relative "../lib/hello_world"

RSpec.describe HelloWorld do
  describe "hello" do
    it "returns the string 'Hello, world!'" do
      expect(HelloWorld.hello).to eq('Hello, world!')
    end
  end
end
